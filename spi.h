#ifndef _SPI_H
#define _SPI_H

#include <stdint.h>
#include <stdio.h>
#include <stm32f4xx_gpio.h>
#include <stm32f4xx_spi.h>

#define SPI_MODE_3      (SPI_CPOL_High | SPI_CPHA_2Edge)
#define SPI_CLK_DIV_2   SPI_BaudRatePrescaler_2

#define SPI             (SPI3)

#define SPI_CS1(x)      (x==0?(GPIOB->BSRRL = GPIO_Pin_9):(GPIOB->BSRRH = GPIO_Pin_9))
#define SPI_CS2(x)      (x==0?(GPIOG->BSRRL = GPIO_Pin_6):(GPIOG->BSRRH = GPIO_Pin_6))

#define SPI_DUMMY_B     (0x00)          //dummy byte to write into data register on read access

#define OPC_READ_DID    (0x9f)          //opcode for reading device id
#define OPC_READ_ARRAY  (0x1b)          //opcode for reading from array

#define OPC_BERASE_4K   (0x20)          //opcode for erasing 4kB block
#define OPC_BERASE_32K  (0x52)          //opcode for erasing 32kB block
#define OPC_BERASE_64K  (0xd8)          //opcode for erasing 64kB block

#define OPC_CERASE      (0x60)          //opcode for chip erase

#define OPC_BPROGRAM    (0x02)          //opcode for byte programming

#define OPC_UNPROTECT   (0x36)

#define OPC_WRITE_EN    (0x06)          //opcode for write enable
#define OPC_WRITE_DIS   (0x04)          //opcode for write disable

#define OPC_READ_SREG   (0x05)          //opcode for reading status register
#define OPC_WRI_SREG1   (0x01)          //opcode for writing status register byte 1
#define OPC_WRI_SREG2   (0x31)          //opcode for writing status register byte 2

#define OPC_RESET       (0xf0)          //opcode for reset

#define SECTOR_SIZE             (0x10000)
#define HALF_SECTOR_SIZE        SECTOR_SIZE/2
#define SIXTEENTH_SECTOR_SIZE   HALF_SECTOR_SIZE/8

void spiInit(void);                     //initialise SPI
int spiChipSelect(int id, int enable);  //select chip to read to / write from, returns -1 in case of invalid id, 0 otherwise
uint8_t spiReadByte(void);              //read one byte of data from the SPI
uint8_t spiWriteByte(uint8_t data);     //write one byte of data to the SPI

int spiCheckDeviceID(uint32_t did);
void printFromSpi(int chipID, uint32_t startAddress, uint32_t nBytes);
uint8_t spiReadData(int chipID, uint32_t startAddress);
void spiEraseData(int chipID, uint32_t startAddress, uint32_t nBytes);
void spiWriteData(int chipID, uint32_t startAddress, char *data);
void spiWriteDataPRBS(int chipID, uint32_t startAddress, uint32_t length);
int32_t spiReadDataPRBS(int chipID, uint32_t startAddress, uint32_t length);


#endif
