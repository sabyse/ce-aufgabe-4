#ifndef _MENU_H
#define _MENU_H

#include <stdint.h>

#define MMAIN       0   //main menu
#define MSUB2       2   //submenu 2: read from device
#define MSUB3       3   //submenu 3: erase from device
#define MSUB4       4   //submenu 4: erase from device
#define MSUB5       5   //submenu 5: write PRBS
#define MSUB6       6   //submenu 6: read PRBS

#define BUFFSIZE    5
#define DATASIZE    256


uint8_t printMainMenu(void);        //prints the main menu, prompts user input and returns the next state
uint8_t printSubMenu2(void);
uint8_t printSubMenu3(void);
uint8_t printSubMenu4(void);
uint8_t printSubMenu5(void);
uint8_t printSubMenu6(void);

#endif
