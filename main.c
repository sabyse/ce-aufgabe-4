#define USE_HW
 
#include <stdint.h>
#include <stdio.h>

#include <stm32f4xx.h>
#include <stm32f4xx_gpio.h>
#include <stm32f4xx_rcc.h>
#include "CE_Lib.h"


#include "spi.h"
#include "menu.h"




int main(void) {
    uint8_t state;
    initCEP_Board();
	// clock setup (enable clock for used ports)
    // =============
    //
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;            // enable clock for GPIOA (RM0090 Chap 6.3.10)
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;            // enable clock for GPIOB
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;            // enable clock for GPIOC
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOFEN;            // enable clock for GPIOF
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOGEN;            // enable clock for GPIOG
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOHEN;            // enable clock for GPIOH
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOIEN;            // enable clock for GPIOI
    spiInit();
    state = MMAIN;
    while(1) {
        switch(state) {
            case MMAIN: {
                state = printMainMenu();
                break;
            }
            case MSUB2: {
                state = printSubMenu2();
                break;
            }
            case MSUB3: {
                state = printSubMenu3();
                break;
            }
            case MSUB4: {
                state = printSubMenu4();
                break;
            }
            case MSUB5: {
                state = printSubMenu5();
                break;
            }
            case MSUB6: {
                state = printSubMenu6();
                break;
            }
        }
    }
}

